<?php
/**
 * @file
 * Provides plugin L.geoJson for Mapping Leaflet.
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('L.geoJson Layer'),
  'description' => t('Provides the Leaflet tyle layer.'),
  'layer_type' => array(
    // This is the file name, not the class itself.
    // File name must end on .class.php, as Ctools standards.
    'class' => 'MappingLeafletGeoJsonLayerType',
  ),
);
