<?php

/**
 * @file
 * Provides L.tileLayer plugin class.
 */

/**
 * L.tileLayer Layer Class.
 *
 * Provides the class for the L.tileLayer plugin.
 */
class MappingLeafletTilelayerLayerType extends MappingLayerType {

  public $layerHandler = 'mappingLeafletTilelayerLayerType';

  /**
   * Default options.
   */
  public function optionsDefault() {
    return array();
  }

  /**
   * Options form.
   * @todo add L.tileLayer options.
   */
  public function optionsForm() {
    return array();
  }

  /**
   * Render the layer.
   */
  public function render(&$element) {
    // Add layer JS.
    $js = drupal_get_path('module', 'mapping_leaflet') . '/plugins/mapping_layer_types/mapping_leaflet_tileLayer_layer_type.js';
    $element['#attached']['js'][] = array(
      'data'   => $js,
      'group' => JS_LIBRARY,
    );
  }
}
