/**
 * @file
 * Mapping Leaflet L.geoJson Layer Type
 */

/**
 * Process Leaflet Layers L.geoJson for Mapping Leaflet module
 *
 * @param layerData
 *   Data about the layer.
 * @param map
 *   Reference to map object.
 *
 * @return
 *   rendered layer for render operation, returns null for other operations.
 */
(function($) {
  Drupal.mapping.layerHandlers.mappingLeafletGeoJsonLayerType = function(layerData, map, op) {
    switch (op) {
      case 'render':
        return L.geoJson(layerData.data.geojson, layerData.data.options);

      case 'init':
        if (map.data.options.layers === undefined) {
          map.data.options.layers = [];
        }
        map.data.options.layers.push(layerData);
        break;

      // Adds this layer to a map.
      case 'add':
        layerData.addTo(map);
        break;

      // Adds geojson data to an existing L.geoJson layer.
      case 'add data':
        // Get the layer by name.
        var layer = Drupal.settings.mapping.layers[layerData.definitions.layer];
        // Check if old data will be erased or not.
        if (layerData.definitions.clear ===  true) {
          layer.clearLayers();
        }
        // Adds data to layer.
        layer.addData(layerData.geojson);

        // Check if map must be centered on layer.
        if (layerData.definitions.recenter === true) {
          // Get the map element by the CSS class name.
          var element = document.getElementsByClassName(map);
          var lmap = $(element).data("mapping").object;
          var bounds = layer.getBounds();
          lmap.fitBounds(bounds);
        }
        break;

    }
  };
})(jQuery);
