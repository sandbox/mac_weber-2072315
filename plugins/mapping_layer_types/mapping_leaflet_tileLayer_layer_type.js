/**
 * @file
 * Mapping Leaflet L.tileLayer Layer Type
 */

/**
 * Process Leaflet Layers L.tileLayer for Mapping Leaflet module
 *
 * @param layerData
 *   Data about the layer.
 * @param map
 *   Reference to map object.
 *
 * @return
 *   rendered layer for render operation, returns null for other operations.
 */
Drupal.mapping.layerHandlers.mappingLeafletTilelayerLayerType = function(layerData, map, op) {
  switch (op) {
    case 'render':
      return L.tileLayer(layerData.data.urlTemplate, layerData.data.options);

    case 'init':
      if (map.data.options.layers === undefined) {
        map.data.options.layers = [];
      }
      map.data.options.layers.push(layerData);
      break;

    case 'add':
      layerData.addTo(map);
      break;
  }
};
