<?php

/**
 * @file
 * Provides L.geoJson plugin class.
 */

/**
 * L.geoJson Layer Class.
 *
 * Provides the class for the L.geoJson plugin.
 */
class MappingLeafletGeoJsonLayerType extends MappingLayerType {

  public $layerHandler = 'mappingLeafletGeoJsonLayerType';

  /**
   * Default options.
   */
  public function optionsDefault() {
    return array(
      'isBaseLayer' => FALSE,
      // GeoJSON array that will be used as the first parameter of L.geoJson().
      // @see http://leafletjs.com/reference.html#geojson
      'geojson' => array(),
      // Options array to be passed to the L.geoJson().
      'options' => '',
    );
  }

  /**
   * Options form.
   * @todo add L.geoJson options.
   */
  public function optionsForm() {
    return array();
  }

  /**
   * Render the layer.
   */
  public function render(&$element) {
    // Add layer JS.
    $js = drupal_get_path('module', 'mapping_leaflet') . '/plugins/mapping_layer_types/mapping_leaflet_geoJson_layer_type.js';
    $element['#attached']['js'][] = array(
      'data'   => $js,
      'group' => JS_LIBRARY,
    );
  }
}
