<?php

/**
 * @file
 * Provides a the class for the base Leaflet map plugin.
 */

/**
 * Leaflet Map Class
 *
 * This provides the base Leaflet map plugin.
 */
class MappingLeafletBaseMapType extends MappingMapType {

  // The JS function handler.
  public $mapHandler = 'mappingLeafletBaseMapType';

  /**
   * Default options.
   */
  public function optionsDefault() {
    return array(
      'options' => array(
        // To render a map at startup there 3 required options:
        // - 'center':  the map center.
        // - 'zoom': zoom level.
        // - 'layers' at least one tile layer is required.
        // Do NOT pass the layers here in the map options, unless it is a full
        // leaflet ILayer object.
        'center' => array(-22.950, -43.138),
        'zoom' => 12,
        // Keyboard Navigation Options.
        // If set to TRUE it may be annoying on pages where the map is not the
        // main content, such as the configuration pages.
        'keyboard' => FALSE,
      ),
    );
  }

  /**
   * Options form.
   */
  public function optionsForm() {
    return array(
      'center' => array(
        '#type' => 'textfield',
        '#title' => t('Map center'),
        '#description' => t('The default center of the map.'),
        '#options' => array(),
        '#default_value' => $this->data['center'],
      ),
    );
  }

  /**
   * Render the map.
   */
  public function render(&$element) {
    // Loads leaflet library.
    libraries_load('leaflet');
    // Adds JS for initializing the map.
    $js = drupal_get_path('module', 'mapping_leaflet') . '/plugins/mapping_map_types/mapping_leaflet_base_map_type.js';
    $element['#attached']['js'][] = array(
      'data'   => $js,
      'group' => JS_LIBRARY,
    );
    // Adds CSS to get leaflet zoom control under Drupal overlay.
    $css = drupal_get_path('module', 'mapping_leaflet') . '/theme/mapping_leaflet.css';
    $element['#attached']['css'][] = $css;
  }
}
