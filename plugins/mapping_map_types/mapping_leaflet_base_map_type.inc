<?php

/**
 * @file
 * Mapping Map Type plugin for the mapping_leaflet module. Provides a basic map
 * type for Leaflet maps.
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('Leaflet base map'),
  'description' => t('Leaflet base map.'),
  'map_type' => array(
    // This is the file name, not the class itself.
    // File name must end on .class.php, as Ctools standards.
    'class' => 'MappingLeafletBaseMapType',
  ),
);
