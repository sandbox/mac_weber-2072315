/**
 * @file
 * Mapping Leaflet Base Map Type
 */

/**
 * Process the initialization of the Leaflet map.
 *
 * @param mapData
 *   Data about the layer.
 * @return
 *   Map object.
 */
Drupal.mapping.mapHandlers.mappingLeafletBaseMapType = function(mapData) {
  // Renders and initialize layers into map definition.
  Drupal.mapping.processLayers(Drupal.settings.mapping.maps[mapData.id].layers, mapData, 'render init');

  // Builds map.
  var map = L.map(mapData.id, mapData.data.options);

  // It is also possible to add layers after building the map:
  // @code
  //Drupal.mapping.processLayers(Drupal.settings.mapping.maps[mapData.id].layers, map, 'render add');
  // @endcode

  return map;
};
