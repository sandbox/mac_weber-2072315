Leaflet Mapping
===============

Provides integration with the Leaflet library using Mapping module.

For users
===============
* Maps, layers, sytles and behaviors are exportable by Ctools.
* All configuration is done outside of Views.
* Compatible with Features module

For developers
===============
* Easy to define maps via hooks - maps can live in code, and even though they
  still can be customized via UI
* Create plugins using Ctools
* Does not depend of geofield - data can be get from geofield or any other
  source
* Uses Libraries module for adding Leaflet library.
